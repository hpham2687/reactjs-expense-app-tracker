import React, { useContext, createContext } from 'react';
import logo from './logo.svg';
import './App.css';
import Balance from './components/Balance';
import History from './components/History';
import AddForm from './components/AddForm';


import {GlobalProvider} from './context/GlobalState'
function App() {
  console.log('')
  return (
    <div className="App">
      <div class="wrapper">
        <GlobalProvider>
          <Balance/>
          <History/>
          <AddForm/>
          </GlobalProvider>
    </div>
    </div>
  );
}

export default App;
