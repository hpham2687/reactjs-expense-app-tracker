import React, { useContext } from 'react';
import {DataContext} from './../context/GlobalState'
function History(props) {
    const {transaction} = useContext(DataContext);
    
    const {dispatch} = useContext(DataContext);
    console.log(transaction)
    const handleDelete = (id) => {
        console.log(id)
        dispatch({type: 'DELETE', payload: id})
    }
    return (
        <>
              <div class="history">
            <div class="history__heading">
                <h1>History</h1>
            </div>
           
            <ul class="list-history">                
            {transaction && transaction.map((value,index)=>{
                return (<><li key={index} class="list-history__item">{value.text}<span>{value.amount}</span>
                                <span onClick={()=>handleDelete(value.id)} className="delete-btn">DELETE</span>

                </li>
                
                </>)
            })}
            </ul>
        </div>
        </>
    );
}

export default History;