import React, { useState, useContext } from 'react';
import { DataContext } from '../context/GlobalState';

function AddForm(props) {
    const [data2Send, setData2Send] = useState({
        textInput: '',
        valueInput: 5
    });
    console.log(data2Send)
    const {dispatch} = useContext(DataContext);

    const handleSubmit = () => {
        console.log(data2Send)
        dispatch({type: 'ADD', payload: data2Send})

    }
    const handleChange = (e) => {
      
        const { name, value } = e.currentTarget;

        setData2Send(prevState=> ({...prevState, [name]: value}))
    }
    return (
        <>
             <div class="add-transaction">
            <div class="history__heading">
                <h1>Add new transaction</h1>
            </div>
            <div class="add-transaction__input-text">
                <span>Text</span>
                <input value={data2Send.textInput} onChange={
                    (e) => handleChange(e)
                    }
                    name="textInput"
               class="input" placeholder="text"/>
            </div>
            <div class="add-transaction__input-value">
                <span>value</span>
                <input onChange={    (e) => handleChange(e)
} value={data2Send.valueInput} name="valueInput" class="input" placeholder="value"/>
            </div>
            <button onClick={()=>handleSubmit()}>Submit</button>
        </div>

        </>
    );
}

export default AddForm;