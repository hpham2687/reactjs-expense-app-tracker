import React, { useContext } from 'react';
import { DataContext } from '../context/GlobalState';

function Balance(props) {
    const {transaction} = useContext(DataContext);
    const amounts = transaction.map(trans => trans.amount);
    const total = amounts.reduce((acc,item)=>(acc+=item))

    const income = amounts
    .filter(item => item > 0)
    .reduce((acc, item) => (acc += item), 0)
    .toFixed(2);

  const expense = (
    amounts.filter(item => item < 0).reduce((acc, item) => (acc += item), 0) *
    -1
  ).toFixed(2);
    return (
        <>
              <h1>Expense Tracker</h1>
        <div class="balance-info">
            Your balance: ${total}            
        </div>
        <div class="income-outcome">
            <div class="income">
                Income
                <span>${income}</span>
            </div>
            <div class="outcome">
                Outcome
                <span>${expense}</span>
            </div>
        </div>
        </>
    );
}

export default Balance;