export const reducer = (state, action) => {
    switch (action.type) {      
        case 'DELETE':
            return {
                ...state, 
                transactions: state.transactions.filter(trans => trans.id!==action.payload)
            } 
        case 'ADD':
            const {payload} = action;
            const {transactions} = state;
            console.log(transactions)
            let newId = transactions[transactions.length-1].id+1;
            let data2Add = [{
                id: newId,
                text:payload.textInput, 
                amount: parseInt(payload.valueInput)
            },... state.transactions]
            
            return {
                ...state,
                transactions: data2Add
            }
        default:
            return state;
    }
}
