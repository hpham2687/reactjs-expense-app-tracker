import React, { useContext, createContext, useReducer } from 'react';
import {reducer} from './Reducer'

const initialState = {
    transactions:   [
        { id: 1, text: 'Flower', amount: -20 },
        { id: 2, text: 'Salary', amount: 300 },
        { id: 3, text: 'Book', amount: -10 },
        { id: 4, text: 'Camera', amount: 150 }
    ]
}
export const DataContext = createContext(initialState);


export const GlobalProvider = ({children}) => {
    const [state, dispatch] = useReducer(reducer, initialState)
    return (<DataContext.Provider 
        value={{
            transaction: state.transactions,
            dispatch: dispatch }}>
        {children}
    </DataContext.Provider>)
}



